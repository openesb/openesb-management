package net.openesb.management.api;

import java.util.Set;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.manage.Task;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface ServiceAssemblyService {
    
    /**
     * returns the list of service asssembly infos that have the
     * service unit deployed on the specified component.
     * 
     * @param state
     *            to return all the service assemblies that are in the specified
     *            state. JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or
     *            null for ANY state
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @return List of service assembly infos
     * @throws ManagementException if error or exception occurs.
     */
    Set<ServiceAssembly> findServiceAssemblies(String state, String componentName) throws ManagementException;
    
    ServiceAssembly getServiceAssembly(String assemblyName) throws ManagementException;
    
    String getDescriptorAsXml(String assemblyName, String serviceUnitName) throws ManagementException;
    
    Task deploy(String serviceAssemblyZipUrl) throws ManagementException;
    
    Task undeploy(String assemblyName, boolean force) throws ManagementException;
    
    Task start(String assemblyName) throws ManagementException;
    
    Task stop(String assemblyName) throws ManagementException;
    
    Task shutdown(String assemblyName, boolean force) throws ManagementException;
}
