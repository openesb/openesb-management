package net.openesb.management.api;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public final class ManagementFactory {
    
    private final static  Logger logger = Logger.getLogger(ManagementFactory.class.getName());
    
    private static ServiceAssemblyService serviceAssemblyService;
    private static SharedLibraryService sharedLibraryService;
    private static ComponentService componentService;
    private static EndpointService endpointService;
    private static AdministrationService administrationService;
    private static MessageService messageService;
    private static StatisticsService statisticsService;
    private static ConfigurationService configurationService;
    private static JvmMetricsService jvmMetricsService;
    
    private ManagementFactory() {
        // Private constructor to avoid instance creation.
    }
    
    public synchronized static ServiceAssemblyService getServiceAssemblyService() {
        if (serviceAssemblyService == null) {
            logger.log(Level.FINEST, "Construct instance of ServiceAssemblyService.");
            
            serviceAssemblyService = 
                    constructInstance("net.openesb.management.jmx.ServiceAssemblyServiceImpl");
        }
        
        return serviceAssemblyService;
    }
    
    public synchronized static SharedLibraryService getSharedLibraryService() {
        if (sharedLibraryService == null) {
            logger.log(Level.FINEST, "Construct instance of SharedLibraryService.");
            
            sharedLibraryService = 
                    constructInstance("net.openesb.management.jmx.SharedLibraryServiceImpl");
        }
        
        return sharedLibraryService;
    }
    
    public synchronized static ComponentService getComponentService() {
        if (componentService == null) {
            logger.log(Level.FINEST, "Construct instance of ComponentService.");
            
            componentService = 
                    constructInstance("net.openesb.management.jmx.ComponentServiceImpl");
        }
        
        return componentService;
    }
    
    public synchronized static EndpointService getEndpointService() {
        if (endpointService == null) {
            logger.log(Level.FINEST, "Construct instance of EndpointService.");
            
            endpointService = 
                    constructInstance("net.openesb.management.jmx.EndpointServiceImpl");
        }
        
        return endpointService;
    }
    
    public synchronized static AdministrationService getAdministrationService() {
        if (administrationService == null) {
            logger.log(Level.FINEST, "Construct instance of AdministrationService.");
            
            administrationService = 
                    constructInstance("net.openesb.management.jmx.AdministrationServiceImpl");
        }
        
        return administrationService;
    }
    
    public synchronized static MessageService getMessageService() {
        if (messageService == null) {
            logger.log(Level.FINEST, "Construct instance of MessageService.");
            
            messageService = 
                    constructInstance("net.openesb.management.jmx.MessageServiceImpl");
        }
        
        return messageService;
    }
    
    public synchronized static StatisticsService getStatisticsService() {
        if (statisticsService == null) {
            logger.log(Level.FINEST, "Construct instance of StatisticsService.");
            
            statisticsService = 
                    constructInstance("net.openesb.management.jmx.StatisticsServiceImpl");
        }
        
        return statisticsService;
    }
    
    public synchronized static ConfigurationService getConfigurationService() {
        if (configurationService == null) {
            logger.log(Level.FINEST, "Construct instance of ConfigurationService.");
            
            configurationService = 
                    constructInstance("net.openesb.management.jmx.ConfigurationServiceImpl");
        }
        
        return configurationService;
    }
    
    public synchronized static JvmMetricsService getJvmMetricsService() {
        if (jvmMetricsService == null) {
            logger.log(Level.FINEST, "Construct instance of JvmMetricsService.");
            
            jvmMetricsService = 
                    constructInstance("net.openesb.management.jmx.JvmMetricsServiceImpl");
        }
        
        return jvmMetricsService;
    }
 
    private static <T> T constructInstance(String serviceClassname) {
        try {
            logger.log(Level.FINE, "Creating instance of {0}", serviceClassname);
            T _instance = (T) Class.forName(serviceClassname).newInstance();
            
            logger.log(Level.FINE, "An instance of {0} has been succesfully created: {1}", new Object []{serviceClassname, _instance});
            return _instance;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Unable to create " + serviceClassname, e);
            return null;
        }
    }
}
