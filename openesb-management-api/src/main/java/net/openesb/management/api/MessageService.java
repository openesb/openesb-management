package net.openesb.management.api;

import java.util.Map;
import net.openesb.model.api.NMR;
import net.openesb.model.api.metric.Metric;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface MessageService {
    
    NMR getNMR() throws ManagementException;
    
    Map<String, Metric> getStatistics() throws ManagementException;
}
