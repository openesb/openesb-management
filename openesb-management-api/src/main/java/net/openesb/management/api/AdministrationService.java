package net.openesb.management.api;

import java.io.File;
import java.util.Set;
import java.util.logging.Level;
import net.openesb.model.api.Instance;
import net.openesb.model.api.Logger;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface AdministrationService {
    
    Instance getInstance() throws ManagementException;
    
    Set<Logger> getLoggers() throws ManagementException;
    
    void setLoggerLevel(String loggerName, Level loggerLevel) throws ManagementException;
    
    File getTempStore();
}
