package net.openesb.management.api;

import java.util.Set;
import net.openesb.model.api.ApplicationConfiguration;
import net.openesb.model.api.ApplicationVariable;
import net.openesb.model.api.ComponentConfiguration;
import net.openesb.model.api.Configuration;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface ConfigurationService {
    
    Set<ComponentConfiguration> getComponentConfiguration(String componentName) throws ManagementException;
    
    void updateComponentConfiguration(String componentName, Set<ComponentConfiguration> configurations) throws ManagementException;
    
    Set<ApplicationVariable> getApplicationVariables(String componentName) throws ManagementException;
    
    void updateApplicationVariable(String componentName, Set<ApplicationVariable> appVariables) throws ManagementException;
    
    void addApplicationVariable(String componentName, Set<ApplicationVariable> appVariables) throws ManagementException;
    
    void deleteApplicationVariables(String componentName, String[] appVariableNames) throws ManagementException;
    
    Configuration getConfigurationSchema(String componentName) throws ManagementException;
    
    Set<String> getApplicationConfigurations(String componentName) throws ManagementException;
    
    ApplicationConfiguration getApplicationConfiguration(String componentName, String configurationName) throws ManagementException;
    
    void deleteApplicationConfiguration(String componentName, String configurationName) throws ManagementException;
    
    void updateApplicationConfiguration(String componentName, ApplicationConfiguration applicationConfiguration) throws ManagementException;
    
    void addApplicationConfiguration(String componentName, ApplicationConfiguration applicationConfiguration) throws ManagementException;
}
