package net.openesb.management.api;

import net.openesb.model.api.manage.Task;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ManagementException extends Exception {

    private Task task;
    
    public ManagementException(String message) {
        super(message);
    }

    public ManagementException(String message, Throwable cause) {
        super(message, cause);
    }

    public ManagementException(Throwable cause) {
        super(cause);
    }
    
    public ManagementException(Task task) {
        this.task = task;
    }
    
    public Task getTask() {
        return this.task;
    }
}
