package net.openesb.management.jmx;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.management.system.ManagementContext;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import javax.management.ObjectName;
import net.openesb.model.api.Instance;
import net.openesb.model.api.Logger;
import net.openesb.management.api.AdministrationService;
import net.openesb.management.api.ManagementException;
import static net.openesb.management.jmx.AbstractServiceImpl.getLogger;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class AdministrationServiceImpl extends AbstractServiceImpl implements AdministrationService {

    @Override
    public Instance getInstance() throws ManagementException {
        Instance instance = new Instance();

        instance.setBuildNumber(getAdminServiceMBean().getBuildNumber());
        instance.setCopyright(getAdminServiceMBean().getCopyright());
        instance.setFullProductName(getAdminServiceMBean().getFullProductName());
        instance.setInstanceName(getAdminServiceMBean().getJbiInstanceName());
        instance.setMajorVersion(getAdminServiceMBean().getMajorVersion());
        instance.setMinorVersion(getAdminServiceMBean().getMinorVersion());
        instance.setShortProductName(getAdminServiceMBean().getShortProductName());
        instance.setSystemInfo(getAdminServiceMBean().getSystemInfo());

        return instance;
    }

    @Override
    public Set<Logger> getLoggers() throws ManagementException {
        ObjectName[] objectNameLoggers = getSystemLoggerMBeans(getAdminServiceMBean().getJbiInstanceName());
        Set<Logger> loggers = new HashSet<Logger>(objectNameLoggers.length);

        for (ObjectName objectName : objectNameLoggers) {
            String loggerName = null;
            String logLevelString = null;
            String logDisplayName = null;

            try {
                getLogger().log(Level.FINE, "loggers: ObjectName = {0}", new Object[]{objectName});

                loggerName = (String) this.invokeMBeanOperation(objectName, "getLoggerName");
                getLogger().log(Level.FINE, "loggers[{0}]: name = {1}", new Object[]{objectName, loggerName});

                logLevelString = (String) this.invokeMBeanOperation(objectName, "getLogLevel");
                getLogger().log(Level.FINE, "level = {0}", logLevelString);

                logDisplayName = (String) this.invokeMBeanOperation(objectName, "getDisplayName");
                getLogger().log(Level.FINE, "displayName = {0}", logDisplayName);

                Logger runtimeLogger = new Logger(loggerName, Level.parse(
                        logLevelString).getName());
                runtimeLogger.setLocalizedLevel(logLevelString);
                runtimeLogger.setDisplayName(logDisplayName);

                loggers.add(runtimeLogger);
            } catch (ManagementRemoteException exception) {
                getLogger().log(Level.SEVERE, "ManagementRemoteException", exception);
            } catch (RuntimeException exception) {
                getLogger().log(Level.SEVERE, "RuntimeException", exception);
            }
        }

        return loggers;
    }

    @Override
    public void setLoggerLevel(String runtimeLoggerName, Level runtimeLoggerLevel) throws ManagementException {
        ObjectName[] objectNameLoggers = getSystemLoggerMBeans(getAdminServiceMBean().getJbiInstanceName());

        for (ObjectName objectName : objectNameLoggers) {
            try {
                getLogger().log(Level.FINE, "loggers: ObjectName = {1}", new Object[]{objectName});

                String loggerName = (String) this.invokeMBeanOperation(objectName, "getLoggerName");

                if (loggerName.equals(runtimeLoggerName)) {
                    getLogger().log(Level.FINE, "Found the matching logger.");
                    this.invokeMBeanOperation(objectName, setLevel(runtimeLoggerLevel));
                }
            } catch (ManagementRemoteException exception) {
                getLogger().log(Level.SEVERE, "ManagementRemoteException", exception);
            } catch (RuntimeException exception) {
                getLogger().log(Level.SEVERE, "RuntimeException", exception);
            }
        }
    }

    @Override
    public File getTempStore() {
        // Just to retrieve the temporary store.
        ManagementContext mc =
                new ManagementContext(getEnvironmentContext());

        return mc.getRepository().getTempStore();
    }
}
