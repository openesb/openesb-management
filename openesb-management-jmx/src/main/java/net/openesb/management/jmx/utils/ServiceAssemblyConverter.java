package net.openesb.management.jmx.utils;

import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.ServiceUnitInfo;
import java.util.HashSet;
import java.util.Set;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.ServiceUnit;
import net.openesb.model.api.State;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ServiceAssemblyConverter {

    public static ServiceAssembly convert(ServiceAssemblyInfo source, boolean includeSUs) {
        ServiceAssembly assembly = new ServiceAssembly();

        assembly.setName(source.getName());
        assembly.setDescription(source.getDescription());
        assembly.setState(State.from(source.getState()));

        if (includeSUs) {
            Set<ServiceUnit> sus = new HashSet<ServiceUnit>(
                    source.getServiceUnitInfoList().size());

            for (Object osuInfo : source.getServiceUnitInfoList()) {
                ServiceUnitInfo suInfo = (ServiceUnitInfo) osuInfo;
                ServiceUnit su = ServiceUnitConverter.convert(suInfo);
                su.setServiceAssembly(source.getName());
                
                sus.add(su);
            }

            assembly.setServiceUnits(sus);
        }

        return assembly;
    }
}
