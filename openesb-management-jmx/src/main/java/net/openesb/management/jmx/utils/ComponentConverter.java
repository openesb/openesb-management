package net.openesb.management.jmx.utils;

import com.sun.jbi.ui.common.JBIComponentInfo;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.openesb.model.api.BindingComponent;
import net.openesb.model.api.JBIComponent;
import net.openesb.model.api.ServiceEngine;
import net.openesb.model.api.State;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ComponentConverter {
    
    public static Set<JBIComponent> convert(String result) {
        List<JBIComponentInfo> componentsInfo = (List<JBIComponentInfo>) JBIComponentInfo.readFromXmlText(result);

        Set<JBIComponent> components = new HashSet<JBIComponent>(componentsInfo.size());

        for (JBIComponentInfo info : componentsInfo) {
            components.add(convert(info));
        }

        return components;
    }

    public static JBIComponent convert(JBIComponentInfo source) {
        JBIComponent component;
        
        if (source.getType().equals(JBIComponentInfo.BINDING_TYPE)) {
            component = new BindingComponent();
        } else if (source.getType().equals(JBIComponentInfo.ENGINE_TYPE)) {
            component = new ServiceEngine();
        } else {
            return null;
        }
        
        component.setName(source.getName());
        component.setDescription(source.getDescription());
        component.setState(State.from(source.getState()));
        component.setBuildNumber(source.getBuildNumber());
        component.setVersion(source.getComponentVersion());
        
        return component;
    }
}
