package net.openesb.management.jmx.utils;

import com.sun.jbi.ui.common.JBIComponentInfo;
import net.openesb.model.api.SharedLibrary;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class SharedLibraryConverter {
    
    public static SharedLibrary convert(JBIComponentInfo source) {
        SharedLibrary library = new SharedLibrary();
        
        library.setName(source.getName());
        library.setDescription(source.getDescription());
        library.setBuildNumber(source.getBuildNumber());
        library.setVersion(source.getComponentVersion());
        
        return library;
    }
}
