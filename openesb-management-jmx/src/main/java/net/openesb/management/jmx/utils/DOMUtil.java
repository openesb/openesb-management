package net.openesb.management.jmx.utils;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class DOMUtil {

    /**
     * replaces the xml entity refereces with the xml escape chars
     *
     * @param xmlString Text with the xml xml escape chars
     * @param Text with the xml entity refereces
     */
    public static String replaceXmlEscapeCharsToEntityReferences(String xmlString) {
        if (xmlString == null) {
            return xmlString;
        }

        // just convert < , > and & only
        StringBuilder sbuff = new StringBuilder(2 * xmlString.length());
        for (int i = 0; i < xmlString.length(); ++i) {
            switch (xmlString.charAt(i)) {
                case '&':
                    sbuff.append("&amp;");
                    break;
                case '<':
                    sbuff.append("&lt;");
                    break;
                case '>':
                    sbuff.append("&gt;");
                    break;
                default:
                    sbuff.append(xmlString.charAt(i));
            }
        }
        return sbuff.toString();
    }
}
