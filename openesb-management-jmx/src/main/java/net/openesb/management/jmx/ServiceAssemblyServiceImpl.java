package net.openesb.management.jmx;

import com.sun.jbi.ui.common.JBIArchive;
import com.sun.jbi.ui.common.ServiceAssemblyDD;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.manage.Task;
import net.openesb.management.api.ManagementException;
import net.openesb.management.api.ServiceAssemblyService;
import static net.openesb.management.jmx.AbstractServiceImpl.getLogger;
import net.openesb.management.jmx.utils.ServiceAssemblyConverter;
import net.openesb.management.jmx.utils.StateUtils;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ServiceAssemblyServiceImpl extends AbstractServiceImpl
        implements ServiceAssemblyService {

    @Override
    public Set<ServiceAssembly> findServiceAssemblies(String state, String componentName) throws ManagementException {
        List<ServiceAssemblyInfo> saInfoList = searchServiceAssemblies(state, componentName, false);

        Set<ServiceAssembly> assemblies = new HashSet<ServiceAssembly>(saInfoList.size());

        for (ServiceAssemblyInfo sAssembly : saInfoList) {
            assemblies.add(ServiceAssemblyConverter.convert(sAssembly, false));
        }

        return assemblies;
    }

    private List<ServiceAssemblyInfo> searchServiceAssemblies(String state, String componentName, boolean includeSUs) throws ManagementException {
        String compName = null;

        if (componentName != null) {
            compName = componentName.trim();
        }

        String[] saNames = new String[0];

        // filter them by component name
        if (compName == null || compName.length() <= 0) {
            getLogger().log(Level.FINE, "Getting all the deployed service assemblies with state {0}", state);

            try {
                saNames = getDeploymentServiceMBean().getDeployedServiceAssemblies();
            } catch (Exception e) {
                getLogger().log(Level.WARNING, "Unable to retrieve assemblies.", e);
            }
        } else {
            getLogger().log(Level.FINE, "Getting all the deployed service assemblies for the component {0}", compName);

            if (false == this.isExistingComponent(compName)) {
                String[] args = {compName};
                throw this.createManagementException(
                        "rest.component.id.does.not.exist", args, null);
            }

            try {
                saNames = getDeploymentServiceMBean().
                        getDeployedServiceAssembliesForComponent(compName);
            } catch (Exception e) {
                getLogger().log(Level.WARNING, "Unable to retrieve assemblies for component.", e);
            }
        }

        // construct service assembly infos from the descriptor
        List<ServiceAssemblyInfo> saInfoByCompNameList = new ArrayList<ServiceAssemblyInfo>();

        for (int i = 0; i < saNames.length; ++i) {
            String saName = saNames[i];

            getLogger().log(Level.FINE, "Getting deployment descriptor for {0}", saName);

            try {
                String saDDText = getDeploymentServiceMBean().getServiceAssemblyDescriptor(saName);

                ServiceAssemblyInfo saInfo = ServiceAssemblyInfo
                        .createFromServiceAssemblyDD(new StringReader(saDDText));

                // update the state of the service assembly from runtime.
                this.updateServiceAssemblyInfoState(saInfo);

                saInfoByCompNameList.add(saInfo);
            } catch (Exception e) {
                getLogger().log(Level.WARNING, "Unable to retrieve assembly descriptor", e);
            }
        }

        state = StateUtils.toFrameworkServiceAssemblyState(state);

        // && filter them by state.
        List<ServiceAssemblyInfo> saInfoList = new ArrayList<ServiceAssemblyInfo>();
        if (state.equalsIgnoreCase("any")) {
            saInfoList.addAll(saInfoByCompNameList);

        } else {
            // filter by specific state
            String uiState = StateUtils.toUiServiceAssemblyState(state);
            for (ServiceAssemblyInfo saInfo : saInfoByCompNameList) {
                if (uiState.equalsIgnoreCase(saInfo.getState())) {
                    saInfoList.add(saInfo);
                }
            }
        }

        if (includeSUs) {
            for (ServiceAssemblyInfo saInfo : saInfoList) {
                this.updateEachServiceUnitInfoStateInServiceAssemblyInfo(saInfo);
            }
        }

        return saInfoByCompNameList;
    }

    @Override
    public ServiceAssembly getServiceAssembly(String assemblyName) throws ManagementException {
        List<ServiceAssemblyInfo> saInfoList = searchServiceAssemblies(null, null, true);

        for (ServiceAssemblyInfo sAssembly : saInfoList) {
            if (sAssembly.getName().equalsIgnoreCase(assemblyName)) {
                return ServiceAssemblyConverter.convert(sAssembly, true);
            }
        }

        String[] args = {assemblyName};
        throw this.createComponentNotFoundException(
                "rest.assembly.id.does.not.exist", args);
    }

    @Override
    public String getDescriptorAsXml(String assemblyName, String serviceUnitName) throws ManagementException {
        checkServiceAssemblyExists(assemblyName);

        try {
            if (serviceUnitName == null) {
                return getDeploymentServiceMBean().getServiceAssemblyDescriptor(assemblyName);
            } else {
                return getDeploymentServiceMBean().getServiceUnitDescriptor(assemblyName, serviceUnitName);
            }
        } catch (Exception e) {
            String[] args = {assemblyName};
            throw this.createManagementException(
                    "rest.assembly.descriptor.error", args, e);
        }
    }

    @Override
    public Task start(String assemblyName) throws ManagementException {
        checkServiceAssemblyExists(assemblyName);

        try {
            String result = getDeploymentServiceMBean().start(assemblyName);
            if (result != null) {
                return getTaskFromString(assemblyName, result);
            }

            return null;
        } catch (Exception e) {
            String[] args = {assemblyName};
            throw this.createManagementException(
                    "rest.assembly.lifecycle.start.error", args, e);
        }
    }

    @Override
    public Task stop(String assemblyName) throws ManagementException {
        checkServiceAssemblyExists(assemblyName);

        try {
            String result = getDeploymentServiceMBean().stop(assemblyName);
            if (result != null) {
                return getTaskFromString(assemblyName, result);
            }

            return null;
        } catch (Exception e) {
            String[] args = {assemblyName};
            throw this.createManagementException(
                    "rest.assembly.lifecycle.stop.error", args, e);
        }
    }

    @Override
    public Task shutdown(String assemblyName, boolean force) throws ManagementException {
        checkServiceAssemblyExists(assemblyName);

        try {
            String result = getDeploymentServiceMBean().shutDown(assemblyName, force);
            if (result != null) {
                return getTaskFromString(assemblyName, result);
            }

            return null;
        } catch (Exception e) {
            String[] args = {assemblyName, Boolean.toString(force)};
            throw this.createManagementException(
                    "rest.assembly.lifecycle.shutdown.error", args, e);
        }
    }

    @Override
    public Task undeploy(String assemblyName, boolean force) throws ManagementException {
        checkServiceAssemblyExists(assemblyName);

        try {
            String result = getDeploymentServiceMBean().undeploy(assemblyName, force);
            if (result != null) {
                return getTaskFromString(assemblyName, result);
            }

            return null;
        } catch (Exception e) {
            String[] args = {assemblyName, Boolean.toString(force)};
            throw this.createManagementException(
                    "rest.assembly.install.undeploy.error", args, e);
        }
    }

    @Override
    public Task deploy(String serviceAssemblyZipUrl) throws ManagementException {
        try {
            String serviceAssemblyName = validateServiceAssembly(serviceAssemblyZipUrl);
            String result = getDeploymentServiceMBean().deploy(serviceAssemblyZipUrl);

            if (result != null) {
                return getTaskFromString(serviceAssemblyName, result);
            }

            return null;
        } catch (Exception e) {
            String[] args = {serviceAssemblyZipUrl};
            throw this.createManagementException(
                    "rest.assembly.install.deploy.error", args, e);
        }
    }

    /**
     * Retrieve the Service Assembly Name
     *
     * @param zipFilePath
     * @return the name of the Service Assembly or null
     */
    private String validateServiceAssembly(String zipFilePath) {
        String name = null;

        try {
            URL url = new URL(zipFilePath);
            JBIArchive archive = new JBIArchive(url.getPath());
            if (archive.isServiceAssemblyArchive() == true) {
                ServiceAssemblyDD descriptor = (ServiceAssemblyDD) archive.getJbiDescriptor();
                name = descriptor.getName();
            }
        } catch (IOException ioException) {
        } catch (Exception exception) {
        }

        return name;
    }

    private void checkServiceAssemblyExists(String serviceAssemblyName) throws ManagementException {
        getServiceAssembly(serviceAssemblyName);
    }
}
