package net.openesb.management.jmx;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import net.openesb.model.api.NMR;
import net.openesb.model.api.Statistic;
import net.openesb.model.api.metric.Metric;
import net.openesb.management.api.ManagementException;
import net.openesb.management.api.MessageService;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class MessageServiceImpl extends AbstractServiceImpl implements MessageService {

    @Override
    public NMR getNMR() throws ManagementException {
        NMR info = new NMR();
        
        info.setEndpointCount(getMessageServiceMBean().getActiveEndpointCount());
        info.setChannels(Arrays.asList(getMessageServiceMBean().getActiveChannels()));
        
        return info;
    }
    
    @Override
    public Map<String, Metric> getStatistics() {
        CompositeData compData = 
                getMessageServiceStatisticsMBean().getMessagingStatistics();
        CompositeType compType = compData.getCompositeType();
        
        Map<String, Metric> statistics = new HashMap<String, Metric>(
                compType.keySet().size());
                
        for (String statKey : compType.keySet()) {
            Object value = compData.get(statKey);
            String description = compType.getDescription(statKey);
            
            statistics.put(statKey, new Statistic<Object> (
                    statKey, value, description));
        }
        
        return Collections.unmodifiableMap(statistics);
    }
}
