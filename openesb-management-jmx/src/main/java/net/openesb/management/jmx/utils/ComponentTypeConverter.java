package net.openesb.management.jmx.utils;

import net.openesb.management.api.ComponentType;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public final class ComponentTypeConverter {

    public static com.sun.jbi.ComponentType convert(ComponentType componentType) {
        if (componentType == null) {
            return null;
        }

        switch (componentType) {
            case ALL:
                return com.sun.jbi.ComponentType.ALL;
            case BINDING:
                return com.sun.jbi.ComponentType.BINDING;
            case BINDINGS_AND_ENGINES:
                return com.sun.jbi.ComponentType.BINDINGS_AND_ENGINES;
            case ENGINE:
                return com.sun.jbi.ComponentType.ENGINE;
            case SHARED_LIBRARY:
                return com.sun.jbi.ComponentType.SHARED_LIBRARY;
            default:
                return null;
        }
    }
}
