package net.openesb.management.jmx.utils;

import com.sun.jbi.ui.common.ServiceUnitInfo;
import net.openesb.model.api.ServiceUnit;
import net.openesb.model.api.State;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ServiceUnitConverter {
    
    public static ServiceUnit convert(ServiceUnitInfo suInfo) {
        ServiceUnit su = new ServiceUnit();
        
        su.setName(suInfo.getName());
        su.setDescription(suInfo.getDescription());
        su.setState(State.from(suInfo.getState()));
        su.setTarget(suInfo.getTargetName());
        su.setDeployedOn(suInfo.getDeployedOn());
        
        return su;
    }
    
    public static ServiceUnit convert(com.sun.jbi.ServiceUnitInfo suInfo) {
        ServiceUnit su = new ServiceUnit();
        
        su.setName(suInfo.getName());
        su.setTarget(suInfo.getTargetComponent());
        su.setDeployedOn(suInfo.getTargetComponent());
        su.setServiceAssembly(suInfo.getServiceAssemblyName());
        su.setState(State.from(suInfo.getStateAsString()));
        
        return su;
    }
}
