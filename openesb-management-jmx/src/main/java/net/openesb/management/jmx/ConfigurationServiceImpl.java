package net.openesb.management.jmx;

import java.io.StringReader;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import javax.management.AttributeList;
import javax.management.MBeanException;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularData;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import net.openesb.model.api.ApplicationConfiguration;
import net.openesb.model.api.ApplicationVariable;
import net.openesb.model.api.ComponentConfiguration;
import net.openesb.model.api.Configuration;
import net.openesb.management.api.ConfigurationService;
import net.openesb.management.api.ManagementException;
import static net.openesb.management.jmx.AbstractServiceImpl.getLogger;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ConfigurationServiceImpl extends AbstractServiceImpl implements ConfigurationService {

    @Override
    public Set<ComponentConfiguration> getComponentConfiguration(String componentName) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isComponentConfigSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.config.not.supported",
                    new String[]{componentName}, null);
        }

        try {
            Map<String, Object> configurations = getComponentConfigurationProperties(componentName);
            Set<ComponentConfiguration> structuredConfigurations = new HashSet<ComponentConfiguration>(configurations.size());

            for (Map.Entry<String, Object> config : configurations.entrySet()) {
                structuredConfigurations.add(
                        new ComponentConfiguration(
                        config.getKey(), config.getValue()));
            }

            return structuredConfigurations;
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }
    }

    @Override
    public Set<ApplicationVariable> getApplicationVariables(String componentName) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isAppVarsSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.vars.not.supported",
                    new String[]{componentName}, null);
        }

        try {
            Map<String, String> variables = retrieveApplicationVariables(componentName);

            Set<ApplicationVariable> structVariables = new HashSet<ApplicationVariable>(variables.size());

            for (Map.Entry<String, String> variable : variables.entrySet()) {
                structVariables.add(
                        new ApplicationVariable(
                        variable.getKey(),
                        componentConfigurationHelper.getAppVarValue(variable.getValue()),
                        componentConfigurationHelper.getAppVarType(variable.getValue(), "string")));
            }

            return structVariables;
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }
    }

    private Map<String, String> retrieveApplicationVariables(String component) throws Exception {
        com.sun.jbi.management.ComponentConfiguration config = getComponentConfigurationMBean(component);

        TabularData datas = config.getApplicationVariables();
        Properties props = componentConfigurationHelper.convertToApplicationVariablesProperties(datas);
        return new HashMap(props);
    }

    @Override
    public void updateComponentConfiguration(String componentName, Set<ComponentConfiguration> configurations) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isComponentConfigSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.config.not.supported",
                    new String[]{componentName}, null);
        }

        Set<ComponentConfiguration> oldConfigurations = getComponentConfiguration(componentName);

        Set<ComponentConfiguration> newConfigurations = new HashSet<ComponentConfiguration>(oldConfigurations.size());
        newConfigurations.addAll(configurations);
        newConfigurations.addAll(oldConfigurations);

        try {
            ObjectName ojbName = getComponentConfigurationMBeanObjectName(componentName);
            AttributeList attrs = constructMBeanAttributes(ojbName, convert(newConfigurations));

            setMBeanConfigAttributes(ojbName, attrs);
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }
    }

    private Map<String, Object> convert(Set<ComponentConfiguration> configurations) {
        Map<String, Object> map = new HashMap<String, Object>(configurations.size());

        for (ComponentConfiguration config : configurations) {
            map.put(config.getName(), config.getValue());
        }

        return map;
    }

    @Override
    public void updateApplicationVariable(String componentName, Set<ApplicationVariable> appVariables) throws ManagementException {
        checkComponentExists(componentName);
        
        getLogger().log(Level.FINE, "updateApplicationVariable({0},{1})",
                new Object[]{componentName, appVariables});

        if (!isAppVarsSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.vars.not.supported",
                    new String[]{componentName}, null);
        }

        //TODO: check component started
        com.sun.jbi.management.ComponentConfiguration configurationMBean =
                getComponentConfigurationMBean(componentName);

        if (appVariables != null) {
            for (ApplicationVariable appVariable : appVariables) {
                try {
                    configurationMBean.setApplicationVariable(
                            appVariable.getName(),
                            convertApplicationVariable(appVariable));
                } catch (Exception ex) {
                    String[] args = {appVariable.getName(), componentName};
                    
                    if (ex instanceof UndeclaredThrowableException) {
                        ex = (Exception)((UndeclaredThrowableException)ex).getUndeclaredThrowable();
                    }

                    throw this.createManagementException(
                            "rest.component.set.app.vars.error", args, ex);
                }
            }
        }
    }

    @Override
    public void addApplicationVariable(String componentName, Set<ApplicationVariable> appVariables) throws ManagementException {
        checkComponentExists(componentName);
        
        getLogger().log(Level.FINE, "addApplicationVariable({0},{1})",
                new Object[]{componentName, appVariables});

        if (!isAppVarsSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.vars.not.supported",
                    new String[]{componentName}, null);
        }

        //TODO: check component started
        com.sun.jbi.management.ComponentConfiguration configurationMBean =
                getComponentConfigurationMBean(componentName);

        if (appVariables != null) {
            for (ApplicationVariable appVariable : appVariables) {
                try {
                    configurationMBean.addApplicationVariable(
                            appVariable.getName(),
                            convertApplicationVariable(appVariable));
                } catch (Exception ex) {
                    String[] args = {appVariable.getName(), componentName};
                    
                    if (ex instanceof UndeclaredThrowableException) {
                        ex = (Exception)((UndeclaredThrowableException)ex).getUndeclaredThrowable();
                    }
                    
                    throw this.createManagementException(
                            "rest.component.add.app.vars.error", args, ex);
                }
            }
        }
    }

    private CompositeData convertApplicationVariable(ApplicationVariable appVariable) {
        try {
            return componentConfigurationHelper.createApplicationVariableComposite(appVariable.getName(), appVariable.getValue(), appVariable.getType());
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, "convertApplicationVariable(" + appVariable + ")", ex);
            return null;
        }
    }

    @Override
    public void deleteApplicationVariables(String componentName, String[] appVariableNames) throws ManagementException {
        checkComponentExists(componentName);
        
        getLogger().log(Level.FINE, "deleteApplicationVariables({0},{1})",
                new Object[]{componentName, appVariableNames});

        if (!isAppVarsSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.vars.not.supported",
                    new String[]{componentName}, null);
        }

        //TODO: check component started
        com.sun.jbi.management.ComponentConfiguration configurationMBean =
                getComponentConfigurationMBean(componentName);

        if (appVariableNames != null) {
            for (String appVariableName : appVariableNames) {
                try {
                    configurationMBean.deleteApplicationVariable(appVariableName);
                } catch (Exception ex) {
                    String[] args = {appVariableName, componentName};
                    throw this.createManagementException(
                            "rest.component.del.app.vars.error", args, ex);
                }
            }
        }
    }

    @Override
    public Configuration getConfigurationSchema(String componentName) throws ManagementException {
        checkComponentExists(componentName);
        
        //TODO: check component started
        com.sun.jbi.management.ComponentConfiguration configurationMBean =
                getComponentConfigurationMBean(componentName);
        try {
            String schema = configurationMBean.retrieveConfigurationDisplayData();

            JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(schema);
            return (Configuration) unmarshaller.unmarshal(reader);
        } catch (Exception e) {
            throw new ManagementException("error : " + e.getMessage());
        }
    }

    private Map<String, Properties> retrieveApplicationConfigurations(String component) throws Exception {
        com.sun.jbi.management.ComponentConfiguration config = getComponentConfigurationMBean(component);

        TabularData datas = config.getApplicationConfigurations();
        return getApplicationConfigurationsMap(datas);
    }

    /**
     * Convert a application configuration TabularData to a Map keyed by the
     * "configurationName". The value is the application configuration
     * represented as properties.
     *
     * @return a Map of application configuration properties
     */
    private Map<String, Properties> getApplicationConfigurationsMap(
            TabularData td) {
        Map<String, Properties> configMap = new HashMap();
        Set configKeys = td.keySet();

        if (configKeys != null) {
            for (Object configKey : configKeys) {
                List keyList = (List) configKey;

                String[] index = new String[keyList.size()];
                index = (String[]) keyList.toArray(index);
                CompositeData cd = td.get(index);

                Properties configProps = this.componentConfigurationHelper
                        .convertCompositeDataToProperties(cd);
                configMap.put(index[0], configProps);
            }
        }
        return configMap;
    }

    @Override
    public Set<String> getApplicationConfigurations(String componentName) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isAppConfigSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.config.not.supported",
                    new String[]{componentName}, null);
        }

        try {
            Map<String, Properties> configurations = retrieveApplicationConfigurations(componentName);
            return configurations.keySet();
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }
    }

    @Override
    public ApplicationConfiguration getApplicationConfiguration(String componentName, String configurationName) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isAppConfigSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.config.not.supported",
                    new String[]{componentName}, null);
        }

        Map<String, Properties> configurationsMap = null;

        try {
            configurationsMap = retrieveApplicationConfigurations(componentName);
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }
        
        Properties appConfigurationProp = configurationsMap.get(configurationName);

        if (appConfigurationProp != null) {
            ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(configurationName);
            
            Set<ComponentConfiguration> configurations = new HashSet<ComponentConfiguration>(appConfigurationProp.size());

            for (final Map.Entry<Object, Object> appConfig : appConfigurationProp.entrySet()) {
                configurations.add(new ComponentConfiguration(
                        appConfig.getKey().toString(), appConfig.getValue()));
            }
            
            applicationConfiguration.setConfigurations(configurations);

            return applicationConfiguration;
        }

        throw this.createManagementException(
                    "rest.component.app.config.does.not.exist",
                    new String[]{componentName, configurationName}, null);
    }

    @Override
    public void deleteApplicationConfiguration(String componentName, String configurationName) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isAppConfigSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.config.not.supported",
                    new String[]{componentName}, null);
        }
        
        if (!checkApplicationConfigurationExists(componentName, configurationName)) {
            throw this.createManagementException(
                    "rest.component.app.config.does.not.exist",
                    new String[]{componentName, configurationName}, null);
        }

        com.sun.jbi.management.ComponentConfiguration config = getComponentConfigurationMBean(componentName);

        try {
            config.deleteApplicationConfiguration(configurationName);
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }
    }

    @Override
    public void updateApplicationConfiguration(String componentName, ApplicationConfiguration appConfiguration) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isAppConfigSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.config.not.supported",
                    new String[]{componentName}, null);
        }

        if (!checkApplicationConfigurationExists(componentName, appConfiguration.getName())) {
            throw this.createManagementException(
                    "rest.component.app.config.does.not.exist",
                    new String[]{componentName, appConfiguration.getName()}, null);
        }
        
        com.sun.jbi.management.ComponentConfiguration config = getComponentConfigurationMBean(componentName);

        try {
            config.setApplicationConfiguration(appConfiguration.getName(), 
                    convertApplicationConfigurations(appConfiguration.getConfigurations()));
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }
    }

    @Override
    public void addApplicationConfiguration(String componentName, ApplicationConfiguration appConfiguration) throws ManagementException {
        checkComponentExists(componentName);
        
        if (!isAppConfigSupported(componentName)) {
            throw this.createManagementException(
                    "rest.component.app.config.not.supported",
                    new String[]{componentName}, null);
        }

        if (checkApplicationConfigurationExists(componentName, appConfiguration.getName())) {
            throw this.createManagementException(
                    "rest.component.app.config.already.exists",
                    new String[]{componentName, appConfiguration.getName()}, null);
        }
        
        com.sun.jbi.management.ComponentConfiguration config = getComponentConfigurationMBean(componentName);

        try {
            config.addApplicationConfiguration(appConfiguration.getName(), 
                    convertApplicationConfigurations(appConfiguration.getConfigurations()));
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            throw new ManagementException(ex.getMessage());
        }   
    }
    
    private Properties convertApplicationConfigurations(Set<ComponentConfiguration> configurations) {
        Properties props = new Properties();
        
        for(ComponentConfiguration config : configurations) {
            props.setProperty(
                    config.getName(), 
                    (config.getValue() != null) ?
                            config.getValue().toString()
                            : "");
        }
                
        return props;
    }
    
    private boolean checkApplicationConfigurationExists(String componentName, String configurationName) {
        try {
            Map<String, Properties> configurations = retrieveApplicationConfigurations(componentName);
            return configurations.containsKey(configurationName);
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, null, ex);
            return false;
        }
    }
}